﻿namespace CanineDotNet.Repository.Interfaces
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}
