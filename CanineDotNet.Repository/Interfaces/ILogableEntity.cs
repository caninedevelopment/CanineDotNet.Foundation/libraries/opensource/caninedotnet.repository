﻿namespace CanineDotNet.Repository.Interfaces
{
    using System;

    public interface ILogableEntity<TKey> : IEntity<TKey>
    {
        DateTime CreatedDate { get; set; }
        DateTime? LastModifiedDate { get; set; }
    }
}
