﻿namespace CanineDotNet.Repository.Interfaces.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ICRUDRepository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : ILogableEntity<TKey>
    {
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<TEntity> GetAsync(TKey id);
        Task CreateAsync(TEntity item);
        Task UpdateAsync(TKey id, TEntity item);
        Task DeleteAsync(TKey id);
    }
}
