﻿namespace CanineDotNet.Repository.Interfaces.Repositories
{
    public interface IRepository<TEntity, TKey> where TEntity : IEntity<TKey>
    {
    }
}
