﻿using CanineDotNet.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CanineDotNet.Repository.Abstracts
{
    public class LogableEntity<TKey> : Entity<TKey>, ILogableEntity<TKey>
    {
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
