﻿
namespace CanineDotNet.Repository.Abstracts
{
    using CanineDotNet.Repository.Interfaces;

    public abstract class Entity<TKey> : IEntity<TKey>
    {
        public TKey Id { get; set; }
    }
}
